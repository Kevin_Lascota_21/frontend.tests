﻿using Frontend.Tests.Models;
using Frontend.Tests.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Tests.Controllers
{
    public class CrescendoController : Controller
    {
        // GET: Crescendo
        public ActionResult Index()
        {
            //use dependency injection
            var recipeService = new RecipeService<Recipe>();

            var recipes = recipeService.GetAllRecipes();

            return View(recipes);
        }

        // GET: Crescendo/Details?recipeId
        public ActionResult Details(string recipeId)
        {
            //use dependency injection
            var recipeService = new RecipeService<Recipe>();

            var recipe = recipeService.GetRecipe(recipeId);

            return View(recipe);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Recipe recipe)
        {
            if (recipe != null)
            {
                //use dependency injection
                var recipeService = new RecipeService<Recipe>();

                recipe.UUID = Guid.NewGuid().ToString();

                recipeService.AddRecipe(recipe);
            }

            return View();
        }

        [HttpGet]
        public ActionResult Update(string recipeId)
        {
            //use dependency injection
            var recipeService = new RecipeService<Recipe>();

            var recipe = recipeService.GetRecipe(recipeId);

            return View(recipe);
        }

        [HttpPost]
        public ActionResult Update(Recipe recipe)
        {
            //use dependency injection
            var recipeService = new RecipeService<Recipe>();

            bool isSuccess = recipeService.UpdateRecipe(recipe);

            if (isSuccess)
            {
                return RedirectToAction("Details", new { recipeId = recipe.UUID });
            }

            return View(recipe);
        }

        public ActionResult Special()
        {
            return View();
        }

        public ActionResult UpdateSpecial()
        {
            //use dependency injection
            var specialService = new SpecialService<Special>();

            var specials = specialService.GetAllSpecials();

            return View(specials);
        }

        [HttpPost]
        public ActionResult UpdateSpecial(Special special)
        {
            //use dependency injection
            var specialService = new SpecialService<Special>();

            bool isSuccess = specialService.UpdateSpecialItem(special);

            if (!isSuccess)
            {
                //log here
            }

            return RedirectToAction("UpdateSpecial");
        }

        // POST: Crescendo/Delete?recipeId
        public ActionResult Delete(string recipeId)
        {
            if (!string.IsNullOrEmpty(recipeId))
            {
                //use dependency injection
                var recipeService = new RecipeService<Recipe>();

                recipeService.DeleteRecipe(recipeId);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public JsonResult GetAllIngredients()
        {
            //use dependency injection
            var recipeService = new RecipeService<Recipe>();

            var recipes = recipeService.GetAllRecipes();

            return Json(recipes, JsonRequestBehavior.AllowGet);
        }

        public static List<Special> GetSpecialIngredient(string ingredientId)
        {
            //use dependency injection
            var specialService = new SpecialService<Special>();

            return specialService.GetSpecialIngredient(ingredientId);
        }

        [HttpGet]
        public JsonResult GetSpecialItem(string id)
        {
            //use dependency injection
            var specialService = new SpecialService<Special>();

            var specialItem = specialService.GetSpecialItem(id);

            return Json(specialItem, JsonRequestBehavior.AllowGet);
        }
    }
}
