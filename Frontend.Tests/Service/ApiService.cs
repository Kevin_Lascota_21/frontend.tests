﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Frontend.Tests.Service
{
    public class ApiService
    {
        private const string ApiUrl= "http://localhost:3001/";

        protected string CallGetApi(string route, string id = "", string parameters = "")
        {
            var webRequest = WebRequest.Create($"{ApiUrl}{route}/{id}?{parameters}");

            var webResponse = (HttpWebResponse)webRequest.GetResponse();

            string response = string.Empty;

            if (webResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var stream = new StreamReader(webResponse.GetResponseStream()))
                {
                    string responseText = stream.ReadToEnd();

                    if (!string.IsNullOrEmpty(responseText))
                    {
                        response = responseText;
                    }
                }
            }

            webResponse.Close();

            return response;
        }

        protected bool CallApi(string route, string data, string method = "POST", string id = "")
        {
            var webRequest = WebRequest.Create($"{ApiUrl}{route}/{id}");
            webRequest.Method = method;
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = data.Length;

            // Write data
            byte[] dataByte = Encoding.ASCII.GetBytes(data);

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(dataByte, 0, dataByte.Length);
            requestStream.Close();

            var webResponse = (HttpWebResponse)webRequest.GetResponse();

            bool isSuccess = false;

            if (webResponse.StatusCode == HttpStatusCode.OK || webResponse.StatusCode == HttpStatusCode.Created)
            {
                using (var stream = new StreamReader(webResponse.GetResponseStream()))
                {
                    string responseText = stream.ReadToEnd();

                    if (!string.IsNullOrEmpty(responseText))
                    {
                        isSuccess = true;
                    }
                }
            }

            webResponse.Close();

            return isSuccess;
        }

        protected bool CallDeleteApi(string route, string id)
        {
            var webRequest = WebRequest.Create($"{ApiUrl}{route}/{id}");
            webRequest.Method = "DELETE";

            var webResponse = (HttpWebResponse)webRequest.GetResponse();

            bool isSuccess = false;

            if (webResponse.StatusCode == HttpStatusCode.OK || webResponse.StatusCode == HttpStatusCode.Created)
            {
                using (var stream = new StreamReader(webResponse.GetResponseStream()))
                {
                    string responseText = stream.ReadToEnd();

                    if (!string.IsNullOrEmpty(responseText))
                    {
                        isSuccess = true;
                    }
                }
            }

            webResponse.Close();

            return isSuccess;
        }
    }
}