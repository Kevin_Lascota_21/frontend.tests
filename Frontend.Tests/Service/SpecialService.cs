﻿using Frontend.Tests.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Frontend.Tests.Service
{
    public class SpecialService<T> : ApiService where T : Special
    {
        private const string Route = "specials";

        public List<T> GetAllSpecials()
        {
            try
            {
                var responseString = CallGetApi(Route);

                return JsonConvert.DeserializeObject<List<T>>(responseString);
            }
            catch (Exception ex)
            {
                //log message
            }

            return null;
        }

        public T GetSpecialItem(string uuid)
        {
            try
            {
                var responseString = CallGetApi(Route, uuid);

                return JsonConvert.DeserializeObject<T>(responseString);
            }
            catch (Exception ex)
            {
                //log message
            }

            return null;
        }

        public bool UpdateSpecialItem(T data)
        {
            try
            {
                var responseString = JsonConvert.SerializeObject(data);
                
                return CallApi(Route, responseString, "PATCH", data.UUID);
            }
            catch (Exception ex)
            {
                //log message
            }

            return false;
        }

        public List<T> GetSpecialIngredient(string ingredientId)
        {
            try
            {
                var responseString = CallGetApi(Route, parameters: $"ingredientId={ingredientId}");

                return JsonConvert.DeserializeObject<List<T>>(responseString);
            }
            catch (Exception ex)
            {
                //log message
            }

            return null;
        }
    }
}