﻿using Frontend.Tests.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Frontend.Tests.Service
{
    /// <summary>
    /// Use Dependency Injection
    /// </summary>
    public class RecipeService<T> : ApiService where T : Recipe
    {
        private const string Route = "recipes";

        public List<T> GetAllRecipes()
        {
            try
            {
                var responseString = CallGetApi(Route);

                return JsonConvert.DeserializeObject<List<T>>(responseString);
            }
            catch (Exception ex)
            {
                //log message
            }

            return null;
        }

        public T GetRecipe(string uuid)
        {
            try
            {
                var responseString = CallGetApi(Route, uuid);

                return JsonConvert.DeserializeObject<T>(responseString);
            }
            catch (Exception ex)
            {
                //log message
            }

            return null;
        }

        public bool AddRecipe(T recipe)
        {
            try
            {
                var responseString = JsonConvert.SerializeObject(recipe);

                return !string.IsNullOrEmpty(responseString) ? CallApi(Route, responseString) : false;
            }
            catch (Exception ex)
            {
                //log message
            }

            return false;
        }

        public bool UpdateRecipe(T recipe)
        {
            try
            {
                var responseString = JsonConvert.SerializeObject(recipe);

                return !string.IsNullOrEmpty(responseString) ? CallApi(Route, responseString, "PATCH", recipe.UUID) : false;
            }
            catch (Exception ex)
            {
                //log message
            }

            return false;
        }

        public bool DeleteRecipe(string recipeId)
        {
            try
            {
                return CallDeleteApi(Route, recipeId);
            }
            catch (Exception ex)
            {
                //log message
            }

            return false;
        }
    }
}