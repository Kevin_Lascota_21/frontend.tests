﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Tests.Formatter
{
    public class CustomDateFormat : IsoDateTimeConverter
    {
        public CustomDateFormat()
        {
            base.DateTimeFormat = "MM/dd/yyyy hh:mm:ss tt";
        }
    }
}