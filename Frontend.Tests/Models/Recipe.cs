﻿using Frontend.Tests.Formatter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Frontend.Tests.Models
{
    public class Recipe
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("images")]
        public Image Images { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("servings")]
        public int Servings { get; set; }

        [JsonProperty("prepTime")]
        public int PrepTime { get; set; }

        [JsonProperty("cookTime")]
        public int CookTime { get; set; }

        [JsonProperty("postDate"), JsonConverter(typeof(CustomDateFormat))]
        public DateTime PostDate { get; set; }

        [JsonProperty("editDate"), JsonConverter(typeof(CustomDateFormat))]
        public DateTime EditDate { get; set; }

        [JsonProperty("ingredients")]
        public List<Ingredient> Ingredients { get; set; }

        [JsonProperty("directions")]
        public List<Direction> Directions { get; set; }
    }
}