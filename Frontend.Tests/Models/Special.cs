﻿using Newtonsoft.Json;

namespace Frontend.Tests.Models
{
    public class Special
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }

        [JsonProperty("ingredientId")]
        public string IngredientId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("geo")]
        public string Geo { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}