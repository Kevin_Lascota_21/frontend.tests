﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Frontend.Tests.Models
{
    public class Ingredient
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("measurement")]
        public string Measurement { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonIgnore]
        public List<Special> Specials { get; set; } = new List<Special>();
    }
}