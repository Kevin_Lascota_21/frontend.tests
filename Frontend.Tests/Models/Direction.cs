﻿using Newtonsoft.Json;

namespace Frontend.Tests.Models
{
    public class Direction
    {
        [JsonProperty("instructions")]
        public string Instructions { get; set; }

        [JsonProperty("optional")]
        public bool Optional { get; set; }
    }
}