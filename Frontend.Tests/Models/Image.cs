﻿using Newtonsoft.Json;

namespace Frontend.Tests.Models
{
    public class Image
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        [JsonProperty("medium")]
        public string Medium { get; set; }

        [JsonProperty("small")]
        public string Small { get; set; }
    }
}